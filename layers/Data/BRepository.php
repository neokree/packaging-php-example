<?php

namespace NeoKree\Package\Layers\Data;

interface BRepository
{
    public function fetch();
}