<?php

namespace NeoKree\Package\Layers\Data;

interface ARepository
{
    public function save($message);
}