<?php

namespace NeoKree\Package\Layers\Controller;

use NeoKree\Package\Layers\Service\AService;

class AController
{
    private $service;

    /**
     * AController constructor.
     * @param AService $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }


    function index() {
        $this->service->saveSomething();
    }
}