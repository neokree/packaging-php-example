<?php

namespace NeoKree\Package\Component\A;

class AComponentImpl implements AComponent
{
    private $repository;

    /**
     * AServiceImpl constructor.
     * @param ARepository $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }


    public function saveSomething()
    {
        $this->repository->save("something");
    }
}