<?php

namespace NeoKree\Package\Component\Web;

use NeoKree\Package\Component\B\BComponent;

class BController
{
    private $service;

    /**
     * BController constructor.
     * @param BComponent $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }


    function index() {
        $this->service->fetchSomething();
    }
}