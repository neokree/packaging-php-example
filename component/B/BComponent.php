<?php

namespace NeoKree\Package\Component\B;

interface BComponent
{
    public function fetchSomething();
}