<?php

namespace NeoKree\Package\Component\B;

class BComponentImpl implements BComponent
{
    private $repository;

    /**
     * BServiceImpl constructor.
     * @param \NeoKree\Package\Component\B\BRepository $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }


    public function fetchSomething()
    {
        $this->repository->fetch();
    }
}