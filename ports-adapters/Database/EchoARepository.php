<?php

namespace NeoKree\Package\PortsAndAdapters\Database;

use NeoKree\Package\PortsAndAdapters\Domain\ARepository;

class EchoARepository implements ARepository
{

    public function save($message)
    {
        echo $message;
    }
}