<?php

namespace NeoKree\Package\PortsAndAdapters\Domain;

class AServiceImpl implements AService
{
    private $repository;

    /**
     * AServiceImpl constructor.
     * @param ARepository $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }


    public function saveSomething()
    {
        $this->repository->save("something");
    }
}