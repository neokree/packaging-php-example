<?php

namespace NeoKree\Package\PortsAndAdapters\Domain;

interface ARepository
{
    public function save($message);
}