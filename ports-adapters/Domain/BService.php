<?php

namespace NeoKree\Package\PortsAndAdapters\Domain;

interface BService
{
    public function fetchSomething();
}