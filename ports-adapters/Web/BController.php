<?php

namespace NeoKree\Package\PortsAndAdapters\Web;

use NeoKree\Package\PortsAndAdapters\Domain\BService;

class BController
{
    private $service;

    /**
     * BController constructor.
     * @param BService $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }


    function index() {
        $this->service->fetchSomething();
    }
}