<?php

namespace NeoKree\Package\Explicit\Ui\B;

use NeoKree\Package\Explicit\Application\B\BService;

class BController
{
    private $service;

    /**
     * BController constructor.
     * @param BService $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }


    function index() {
        $this->service->fetchSomething();
    }
}