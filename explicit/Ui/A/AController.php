<?php

namespace NeoKree\Package\Explicit\Ui\A;

use NeoKree\Package\Explicit\Application\A\AService;

class AController
{
    private $service;

    /**
     * AController constructor.
     * @param \NeoKree\Package\Explicit\Application\A\AService $service
     */
    public function __construct($service)
    {
        $this->service = $service;
    }


    function index() {
        $this->service->saveSomething();
    }
}