<?php

namespace NeoKree\Package\Explicit\Infrastructure\B;

use NeoKree\Package\Explicit\Application\Ports\BRepository;

class FakeBRepository implements BRepository
{

    public function fetch()
    {
        return "something";
    }
}