<?php

namespace NeoKree\Package\Explicit\Infrastructure\A;

use NeoKree\Package\Explicit\Application\Ports\ARepository;

class EchoARepository implements ARepository
{

    public function save($message)
    {
        echo $message;
    }
}