<?php

namespace NeoKree\Package\Explicit\Application\B;

use NeoKree\Package\Explicit\Application\Ports\BRepository;

class BServiceImpl implements BService
{
    private $repository;

    /**
     * BServiceImpl constructor.
     * @param BRepository $repository
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
    }


    public function fetchSomething()
    {
        $this->repository->fetch();
    }
}