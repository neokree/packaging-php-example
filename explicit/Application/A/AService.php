<?php

namespace NeoKree\Package\Explicit\Application\A;

interface AService
{
    public function saveSomething();
}