<?php

namespace NeoKree\Package\Explicit\Application\Ports;

interface ARepository
{
    public function save($message);
}